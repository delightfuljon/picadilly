// lib/rss.js

const fs = require("fs");
const RSS = require("rss");
const path = require("path");

function generateFeed(imageFiles) {
  const feed = new RSS({
    title: "Image Gallery",
    description: "An RSS feed of images from the gallery",
    feed_url: "http://picadilly.vercel.app/feed.xml",
    site_url: "http://picadilly.vercel.app",
  });

  imageFiles.sort((a, b) => {
    const dateA = a.split("]")[0].substring(1);
    const dateB = b.split("]")[0].substring(1);
    return new Date(dateB) - new Date(dateA);
  });

  imageFiles.forEach((file) => {
    if (file.includes("]")) {
      const date = file.split("]")[0].substring(1);
      const title = file.split("]")[1].split(".")[0];

      feed.item({
        title: title,
        description: `<img src="http://picadilly.vercel.app/gallery/${file}" />`,
        url: `http://picadilly.vercel.app/gallery/${file}`,
        date: date,
      });
    }
  });

  const xml = feed.xml({ indent: true });

  fs.writeFileSync(path.join(process.cwd(), "public", "feed.xml"), xml);
}

module.exports = {
  generateFeed,
};
