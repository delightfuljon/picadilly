import ImageGrid from "../components/ImageGrid";
import path from "path";
import fs from "fs";

export default function Home({ images }) {
  return <ImageGrid images={images} />;
}

export async function getStaticProps() {
  const imagesDirectory = path.join(process.cwd(), "public/gallery/");
  const filenames = fs.readdirSync(imagesDirectory).filter((filename) => {
    const filePath = path.join(imagesDirectory, filename);
    return fs.statSync(filePath).isFile();
  });
  const images = filenames.map((filename) => ({
    src: `/gallery/${filename}`,
    alt: filename,
  }));

  return {
    props: {
      images,
    },
  };
}
