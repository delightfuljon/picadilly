import React from "react";

export default function Image({ src, alt }) {
  return (
    <div style={{ width: "100%", position: "relative" }}>
      <img
        src={src}
        alt={alt}
        style={{
          objectFit: "cover",
          width: "100%",
          height: "100%",
        }}
      />
    </div>
  );
}
