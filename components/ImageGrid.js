import React from "react";
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry";
import Image from "./Image";

export default function ImageGrid({ images }) {
  return (
    <ResponsiveMasonry columnsCountBreakPoints={{ 350: 1, 750: 2, 900: 5 }}>
      <Masonry>
        {[...images].reverse().map((image, index) => (
          <div key={index} className="image-wrapper">
            <Image src={image.src} alt={image.alt} />
          </div>
        ))}
      </Masonry>
    </ResponsiveMasonry>
  );
}
