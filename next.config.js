// next.config.js

module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["localhost"],
  },
  webpack: (config, { isServer }) => {
    if (isServer) {
      const path = require("path");
      const fs = require("fs");

      const imagesDir = path.join(process.cwd(), "public", "gallery");
      const imageFiles = fs.readdirSync(imagesDir);

      const rss = require("./lib/rss");
      rss.generateFeed(imageFiles);
    }

    return config;
  },
};
